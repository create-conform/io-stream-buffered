/////////////////////////////////////////////////////////////////////////////////////////////
//
// io-stream-buffered
//
//    IO Buffered Stream Class.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var Error =     require("error");
var event =     require("event");
var type  =     require("type");
var stream =    require("io-stream");

/////////////////////////////////////////////////////////////////////////////////////////////
//
// StreamBuffered Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function StreamBuffered(buffer, name) {
    var own = this;
    var closed = false;
    var events = new event.Emitter(this);

    this.getName = function () {
        return name;
    };
    this.getLength = function () {
        return new Promise(function (resolve, refuse) {
            closed ? refuse(new Error(stream.ERROR_STREAM_CLOSED, "")) : resolve(buffer.length);
        });
    };
    this.read = function (len, position) {
        if (!position) {
            position = 0;
        }

        return new Promise(function (resolve, refuse) {
            if (closed) {
                refuse(new Error(stream.ERROR_STREAM_CLOSED, ""));
                return;
            }
            if (len == null) {
                return own.getLength().then(function (length) {
                    doRead(length - position);
                }).catch(refuse);
            }
            else {
                doRead(len);
            }

            function doRead(len) {
                if (len == buffer.length) {
                    resolve(buffer);
                }
                else {
                    var nBuf = buffer.subarray(position, position + len);
                    position += len;
                    resolve(nBuf);
                }
            }
        });
    };
    this.write = function (data, position) {
        if (!position) {
            position = 0;
        }

        return new Promise(function (resolve, refuse) {
            if (closed) {
                refuse(new Error(stream.ERROR_STREAM_CLOSED, ""));
                return;
            }

            if (data == null) {
                resolve();
                return;
            }
            if (!((typeof Buffer != "undefined" && data instanceof Buffer) || (typeof Uint8Array != "undefined" && data instanceof Uint8Array) || type.isString(data))) {
                refuse(new Error(Error.ERROR_INVALID_PARAMETER, "Mandatory parameter 'data' should be of type 'Buffer', 'Uint8Array' or 'String'."));
                return;
            }
            if (!type.isFunction(data.toString)) {
                refuse(new Error(Error.ERROR_INVALID_PARAMETER, "Mandatory parameter 'data' should have the toString() function."));
                return;
            }
            var newData;
            var newB;
            var newLength;

            newData = data instanceof Uint8Array ? data : data.toUint8Array();
            newLength = position + newData.length;
            newB = new Uint8Array(newLength);
            newB.set(buffer, 0);
            newB.set(newData, position);

            buffer = newB;
            resolve();
        });
    };
    this.close = function () {
        return new Promise(function (resolve, refuse) {
            if (closed) {
                refuse(new Error(stream.ERROR_STREAM_CLOSED, ""));
                return;
            }

            buffer = null;

            closed = true;
            resolve();
        });
    };
};
StreamBuffered.prototype = stream;

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = StreamBuffered;